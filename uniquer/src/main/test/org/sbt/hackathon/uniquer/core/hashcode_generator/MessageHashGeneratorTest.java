package org.sbt.hackathon.uniquer.core.hashcode_generator;

import net.jpountz.xxhash.StreamingXXHash64;
import org.junit.Test;
import org.sbt.hackathon.uniquer.core.message_converter.MessageConverter;
import org.sbt.hackathon.uniquer.core.message_converter.MessageConverterImpl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class MessageHashGeneratorTest {
    public static final int BYTE_ARRAY_SIZE = 100 * 1024 * 1024;
    public MessageConverter messageConverter = new MessageConverterImpl();
    public MessageHashGenerator messageHashGenerator = new MessageHashGeneratorImpl();

    @Test
    public void perfTest() throws IOException {
        byte[] data = new byte[BYTE_ARRAY_SIZE];
        new Random().nextBytes(data);
        long s =0;

        s = System.nanoTime();
        long[] longs = messageConverter.toLongArray(data);;
        long r2 = System.nanoTime() - s;
        System.out.println(r2);

        s = System.nanoTime();
        int i1 = Arrays.hashCode(longs);
        long r3 = System.nanoTime() - s;
        System.out.println(r3);

        s = System.nanoTime();
        long i3 = messageHashGenerator.generateHashCode(data);
        long r1 = System.nanoTime() - s;
        System.out.println(r1);

    }
}