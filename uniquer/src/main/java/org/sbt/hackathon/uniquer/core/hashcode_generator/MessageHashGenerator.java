package org.sbt.hackathon.uniquer.core.hashcode_generator;

public interface MessageHashGenerator {
    long generateHashCode(byte[] messageBytes);
}
