package org.sbt.hackathon.uniquer.core.message_node;

import java.util.LinkedList;
import java.util.Objects;

public class MessagesNode {
    public final long messageHash;
    public final LinkedList<byte[]> messages;

    public MessagesNode(long messageHash, byte[] firstMessage) {
        this.messageHash = messageHash;
        this.messages = new LinkedList<>();
        this.messages.add(firstMessage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessagesNode that = (MessagesNode) o;
        return messageHash == that.messageHash;
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageHash);
    }
}
