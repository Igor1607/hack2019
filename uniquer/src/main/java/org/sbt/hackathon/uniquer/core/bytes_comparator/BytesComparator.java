package org.sbt.hackathon.uniquer.core.bytes_comparator;

import java.nio.ByteBuffer;

public interface BytesComparator {
    boolean compare(byte[] first, byte[] second);

    boolean compare(ByteBuffer first, ByteBuffer second);

    boolean compare(long[] first, long[] second);
}
