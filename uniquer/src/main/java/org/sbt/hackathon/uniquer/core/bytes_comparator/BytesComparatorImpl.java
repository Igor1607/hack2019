package org.sbt.hackathon.uniquer.core.bytes_comparator;

import org.sbt.hackathon.uniquer.core.message_converter.MessageConverter;
import org.sbt.hackathon.uniquer.core.message_converter.MessageConverterImpl;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class BytesComparatorImpl implements BytesComparator {
    private Comparators comparators = new Comparators();
    private MessageConverter messageConverter = new MessageConverterImpl();

    public boolean compare(byte[] first, byte[] second) {
        return Arrays.equals(first, second);
    }

    public boolean compare(ByteBuffer first, ByteBuffer second) {
        return compare(messageConverter.toLongArray(first), messageConverter.toLongArray(second));
    }

    @Override
    public boolean compare(long[] first, long[] second) {
        return comparators.simpleLongsCompare(first, second); // simple compare for init
    }
}
