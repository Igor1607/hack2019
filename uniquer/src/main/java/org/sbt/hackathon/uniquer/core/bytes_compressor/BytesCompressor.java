package org.sbt.hackathon.uniquer.core.bytes_compressor;

public interface BytesCompressor {
    byte[] compress(byte[] incomeBytes);
}
