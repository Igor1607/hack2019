package org.sbt.hackathon.uniquer.core.message_converter;

import java.nio.ByteBuffer;

public interface MessageConverter {
    long[] toLongArray(byte[] byteArray);

    long[] toLongArray(ByteBuffer byteBuffer);

    byte[] toByteArray(long longValue);
}
