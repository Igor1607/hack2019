package org.sbt.hackathon.uniquer.core.message_converter;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;

public class MessageConverterImpl implements MessageConverter {
    @Override
    public long[] toLongArray(byte[] byteArray) {
        int idx = 0, offset = Long.BYTES;
        long[] result = new long[getArraySize(byteArray.length)];
        while (idx < result.length) {
            long next = 0;
            for (int i = idx * offset; i < (idx + 1) * offset && i < byteArray.length; i++) {
//                next += (bytes[i] & 0xff) << (offset * (i % offset));
                next = (next << offset) | (byteArray[i] & 0xff);
            }
            result[idx++] = next;
        }
        return result;
    }

    @Override
    public long[] toLongArray(ByteBuffer byteBuffer) {
        LongBuffer longBuffer = byteBuffer.asLongBuffer();
        long[] array = new long[longBuffer.capacity()];
        longBuffer.get(array);
        return array;
    }

    @Override
    public byte[] toByteArray(long longValue) {
        byte[] result = new byte[Long.BYTES];
        for (int i = Long.BYTES - 1; i >= 0; --i) {
            result[i] = (byte)(longValue & 0xFF);
            longValue >>= 8;
        }
        return result;
    }

    // размер массива лонгов, относительно кратности 8-ми байтового буфера
    private int getArraySize(int bytesLength) {
        int offset = Long.BYTES;
        return (bytesLength % offset == 0) ?
                bytesLength / offset : bytesLength / offset + 1;
    }

}
