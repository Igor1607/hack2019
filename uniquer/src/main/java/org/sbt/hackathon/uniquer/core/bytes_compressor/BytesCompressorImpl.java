package org.sbt.hackathon.uniquer.core.bytes_compressor;

import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;

public class BytesCompressorImpl implements BytesCompressor {
    private LZ4Factory factory = LZ4Factory.fastestInstance();
    private LZ4Compressor compressor = factory.fastCompressor();

    @Override
    public byte[] compress(byte[] incomeBytes) {
        return compressor.compress(incomeBytes);
    }
}
