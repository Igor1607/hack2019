package org.sbt.hackathon.uniquer.core.message_uniquer;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.sbt.hackathon.uniquer.core.bytes_comparator.BytesComparator;
import org.sbt.hackathon.uniquer.core.bytes_comparator.BytesComparatorImpl;
import org.sbt.hackathon.uniquer.core.bytes_compressor.BytesCompressor;
import org.sbt.hackathon.uniquer.core.bytes_compressor.BytesCompressorImpl;
import org.sbt.hackathon.uniquer.core.hashcode_generator.MessageHashGenerator;
import org.sbt.hackathon.uniquer.core.hashcode_generator.MessageHashGeneratorImpl;
import org.sbt.hackathon.uniquer.core.message_converter.MessageConverter;
import org.sbt.hackathon.uniquer.core.message_converter.MessageConverterImpl;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class MessageUniquerImpl implements MessageUniquer {
    private volatile Jedis jedis;

    private LoadingCache<Long, List<byte[]>> msgCache = CacheBuilder.newBuilder()
            .maximumSize(10000)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build(new CacheLoader<Long, List<byte[]>>() {
                @Override
                public List<byte[]> load(Long hash) throws Exception {
                    return new ArrayList<>();
                }
            });

    public MessageUniquerImpl(String redisHost) {
        this.jedis = new Jedis(redisHost);
    }

    public MessageUniquerImpl(String redisHost, int port) {
        this.jedis = new Jedis(redisHost, port);
    }

    public MessageUniquerImpl(String redisHost, int port, int sessionTimeout) {
        this.jedis = new Jedis(redisHost, port, sessionTimeout);
    }

//    private BytesCompressor bytesCompressor = new BytesCompressorImpl();
    private MessageHashGenerator messageHashGenerator = new MessageHashGeneratorImpl();
    private BytesComparator bytesComparator = new BytesComparatorImpl();
    private MessageConverter converter = new MessageConverterImpl();

    public void isMessageUnique(byte[] incomeMessage, Runnable onSuccess, Runnable onRollback) {
        long incomeMessageHashcode = messageHashGenerator.generateHashCode(incomeMessage);
        byte[] hashBytes = converter.toByteArray(incomeMessageHashcode);

        jedis.watch(hashBytes);
        List<byte[]> messagesInRedis = msgCache.getIfPresent(incomeMessageHashcode);
        if (messagesInRedis == null) {
            messagesInRedis = getMessagesInRedis(hashBytes);
        }

        Transaction transaction = jedis.multi();
        for (byte[] messageInRedis : messagesInRedis) {
            if (bytesComparator.compare(messageInRedis, incomeMessage)) {
                transaction.discard();
                msgCache.put(incomeMessageHashcode, messagesInRedis);
                onRollback.run();
                return;
            }
        }

        transaction.rpush(hashBytes, incomeMessage);
        transaction.incrBy("offset", incomeMessage.length);
        List<Object> isExecuted = transaction.exec();
        if(isExecuted != null) {
            onSuccess.run();
        } else {
            onRollback.run();
        }
    }

    @Override
    public long getCurrentOffset() {
        String offset = jedis.get("offset");
        return offset != null ? Long.parseLong(offset) : 0;
    }

    private List<byte[]> getMessagesInRedis(byte[] hashBytes) {
        return jedis.lrange(hashBytes, 0, 100);
    }
}
