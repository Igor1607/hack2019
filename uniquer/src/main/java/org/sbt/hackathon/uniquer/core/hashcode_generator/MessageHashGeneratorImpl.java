package org.sbt.hackathon.uniquer.core.hashcode_generator;

import net.jpountz.xxhash.XXHash64;
import net.jpountz.xxhash.XXHashFactory;

public class MessageHashGeneratorImpl implements MessageHashGenerator {
    private XXHashFactory factory = XXHashFactory.fastestInstance();

    @Override
    public long generateHashCode(byte[] messageBytes) {
        int seed = 0x9747b28c; // used to initialize the hash value, use whatever
        // value you want, but always the same
        XXHash64 hash64 = factory.hash64();
        return hash64.hash(messageBytes, 0, messageBytes.length, seed);
    }
}
