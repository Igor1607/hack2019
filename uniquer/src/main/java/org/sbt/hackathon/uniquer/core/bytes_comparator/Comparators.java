package org.sbt.hackathon.uniquer.core.bytes_comparator;

import java.util.Arrays;

class Comparators {

    public boolean simpleBytesCompare(byte[] first, byte[] second) {
        return Arrays.equals(first, second);
    }

    public boolean simpleLongsCompare(long[] first, long[] second) {
        if(first == null || second == null || first.length != second.length) {
            return false;
        }
        if (first == second) return true;
        for (int i = 0; i < first.length; i++) {
            if (0L != (first[i] ^ second[i])) {
                return false;
            }
        }
        return true;
    }
}
