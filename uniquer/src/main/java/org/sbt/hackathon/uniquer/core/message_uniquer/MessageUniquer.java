package org.sbt.hackathon.uniquer.core.message_uniquer;

public interface MessageUniquer {
    void isMessageUnique(byte[] incomeMessage, Runnable onSuccess, Runnable onRollback);

    long getCurrentOffset();
}
