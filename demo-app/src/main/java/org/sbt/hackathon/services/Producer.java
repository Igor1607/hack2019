package org.sbt.hackathon.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static Logger logger = LoggerFactory.getLogger(Producer.class);

    private final KafkaTemplate<String, byte[]> messageTemplate;

    public Producer(KafkaTemplate<String, byte[]> messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public void publish(byte[] msg) {
        messageTemplate.send("team6-test-output", msg);
    }
}
