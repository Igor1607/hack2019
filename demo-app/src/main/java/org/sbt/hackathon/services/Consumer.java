package org.sbt.hackathon.services;

import org.apache.kafka.common.TopicPartition;
import org.sbt.hackathon.properties.AppProperties;
import org.sbt.hackathon.uniquer.core.message_uniquer.MessageUniquer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class Consumer implements ConsumerSeekAware {
    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private int doubles = 0;
    private int messages = 0;
    private final MessageUniquer uniquer;
    private final Producer producer;
    private final AppProperties properties;
    private boolean firstStart = true;
    private long start = System.currentTimeMillis();

    public Consumer(MessageUniquer uniquer, Producer producer, AppProperties properties) {
        this.uniquer = uniquer;
        this.producer = producer;
        this.properties = properties;
    }


    @KafkaListener(topics = "input")
    public void consume(List<byte[]> messagesList) {
        for (byte[] msg : messagesList) {
            messages++;
            uniquer.isMessageUnique(msg, () -> {
                producer.publish(msg);
            }, () -> {
                doubles++;
                if (doubles % 10000 == 0 || doubles > 122000) {
                    logger.info("{} duplicates processed", doubles);
                }
            });
            if (messages % 10000 == 0){
                long time = System.currentTimeMillis() - start;
                logger.info("{} messages processed. Used {} millis for 10k messages", messages, time);
                start = System.currentTimeMillis();
            }
        }
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        if (firstStart && properties.isResetOffset()){
            logger.info("Reset offsets");
            assignments.keySet().forEach(partition -> callback.seekToBeginning(partition.topic(), partition.partition()));
            firstStart = false;
        }
    }
}
