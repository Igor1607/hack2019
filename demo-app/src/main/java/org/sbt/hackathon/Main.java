package org.sbt.hackathon;

import org.sbt.hackathon.properties.UniquerProperties;
import org.sbt.hackathon.uniquer.core.message_uniquer.MessageUniquer;
import org.sbt.hackathon.uniquer.core.message_uniquer.MessageUniquerImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@SpringBootApplication
@EnableKafka
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public KafkaTemplate<String, byte[]> messageTemplate(ProducerFactory<String, byte[]> factory) {
        return new KafkaTemplate<>(factory);
    }

    @Bean
    public MessageUniquer uniquer(UniquerProperties properties){
        return new MessageUniquerImpl(properties.getRedisHost(), properties.getRedisPort());
    }
}
