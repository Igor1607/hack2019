package org.sbt.hackathon.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "org.sbt.app")
public class AppProperties {
    private boolean resetOffset = false;

    public boolean isResetOffset() {
        return resetOffset;
    }

    public void setResetOffset(boolean resetOffset) {
        this.resetOffset = resetOffset;
    }
}
